#include<iostream>
#include<cstdlib>
#include<ctime>
#include<iomanip>
using namespace std;
void rand_int(int );
void sum(int , int []);
void high(int , int []);
void low(int , int []);
void mean(float );
void median(int , int []);
void mode(int , int []);
void even(int , int []);
void odd(int , int []);
void bubble_sort(int , int []);
int main()
{
	int num;
	srand(time(0));
	num = rand() % 101 + 50;
	cout << "          CHALLENGE #4" << endl;
	cout << "The number of elements       = " << num;
	int rnd[num] = {};
	rand_int(num);
	sum(num, rnd);
	high(num,rnd);
	low(num,rnd);
	mean(num);
	median(num,rnd);
	mode(num,rnd);
	even(num,rnd);
	odd(num,rnd);
	bubble_sort(num,rnd);
}
void rand_int(int num)
{
	int i, sum, rnd[num] = {};
	for(i=0;i<num;i++)
	{
		rnd[i] = rand() % 100;
	}
}
void sum(int num, int rnd[])
{
	int i, sum = 0;
	for(i=0;i<num;i++)
	{
		rnd[i] = rand() % 100;
		sum = sum + rnd[i];
	}
	cout << endl << "The sum of all elements      = " << sum;
}
void low(int num, int rnd[])
{
	int i, low = 100;
	for(i=0;i<num;i++)
	{
		if(rnd[i] < low)
		{
			low = rnd[i];
		}
	}
	cout << endl << "The lowest value             = " << low;
}
void high(int num, int rnd[])
{
	int i, high = 0;
	for(i=0;i<num;i++)
	{
		if(rnd[i] > high)
		{
			high = rnd[i];
		}
	}
	cout << endl << "The highest value            = " << high;
}
void even(int num, int rnd[])
{
	int i, even = 0;
	for(i=0;i<num;i++)
	{
		if(rnd[i] % 2 == 0)
		{
			even++;
		}
	}
	cout << endl << "The number of even numbers   = " << even;
}
void odd(int num, int rnd[])
{
	int i, odd = 0;
	for(i=0;i<num;i++)
	{
		if(rnd[i] % 2 != 0)
		{
			odd++;
		}
	}
	cout << endl << "The number of odd numbers    = " << odd;
}
void mean(float num)
{
	float mean;
	mean = num / 2;
	cout << endl << "The mean value               = " << mean;
}
void median(int num, int rnd[])
{
	int i, j, flag = 1, temp, median1, median2;
	float median;
	for(i=0;i<num;i++)
	{
		flag = 0;
		for(j=0;j<num-1;j++)
    	{
    		if(rnd[j+1] < rnd[j])
       		{ 
            	temp = rnd[j];
            	rnd[j] = rnd[j+1];
            	rnd[j+1] = temp;
            	flag = 1;
        	}
    	}
	}
	if(num % 2 != 0)
	{
		median1 = rnd[((num+1)/2)-1];
		cout << endl << "The median value             = " << median1;
	}
	if(num % 2 == 0)
	{
		median1 = rnd[(num/2)-1];
		median2 = rnd[(num/2)];
		median = (median1 + median2)/2;
		cout << endl << "The median value             = " << median;
	}
}
void mode(int num, int rnd[])
{
	int i, j, flag = 1, temp, mode[num]= {}, mod;
	for(i=0;i<num;i++)
	{
		flag = 0;
		for(j=0;j<num-1;j++)
    	{
    		if(rnd[j+1] < rnd[j])
       		{ 
            	temp = rnd[j];
            	rnd[j] = rnd[j+1];
            	rnd[j+1] = temp;
            	flag = 1;
        	}
    	}
	}
	for(i=0;i<num;i++)
	{
		if(rnd[i] == rnd[i+1])
		{
			mode[i]++;
		}
	}
	/*for(i=0;i<num;i++)
	{
		if(i % 10 == 0)
		{
			cout << endl;
		}
		cout << mode[i] << " ";
	}*/
}
void bubble_sort(int num, int rnd[])
{
	int i, j, flag = 1, temp;
	cout << endl << "The values output in order from lowest to highest : ";
	for(i=0;i<num;i++)
	{
		flag = 0;
		for(j=0;j<num-1;j++)
    	{
    		if(rnd[j+1] < rnd[j])
       		{ 
            	temp = rnd[j];
            	rnd[j] = rnd[j+1];
            	rnd[j+1] = temp;
            	flag = 1;
        	}
    	}
	}
    for(i=0;i<num;i++)
	{
		if(i % 10 == 0)
		{
			cout << endl;
		}
		cout << setw(5) << rnd[i];
	}
}

